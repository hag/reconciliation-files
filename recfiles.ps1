# This is a small module to retrive all files under the Root folder and save them into an csv
#If we want to exlude folders then we need to add | where {!$_.PSIsContainer} | 

$includeFoldders = Read-Host 'Do you want to include folder names(y/n)[Default no]?'

$fileName =  $PSScriptRoot | Split-Path -Leaf 
if(($fileName -replace "[^\s]").length){
  $fileName = $fileName -replace '\s','_'
  Write-Host  "Renamed exported fileName: $($fileName)"
}ELSE{
  Write-Host  "Exported fileName: $($fileName)"
}
Write-Host  "Include folder names: $('y' -ieq $includeFoldders)"

#? {$_.PSIsContainer -eq ('y' -ieq $includeFoldders) } 
$t = Measure-Command {
get-childitem -rec -force  |
?  {($_.PSIsContainer -eq $('y' -ieq $includeFoldders)) -or (!$_.PSIsContainer)} |

select-object FullName, Length | export-csv -Encoding UTF8 -notypeinformation -delimiter ',' -path $fileName".csv" }
Write-Host "It took $("$t" -replace '^\d+?\.') to find all files."
