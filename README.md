Depending on the option this script could retrieve all the files with the length in bytes.

# How to run

![Alt text](recfiles-example.JPG?raw=true 'Running the script')

```powershell
.\recfiles.ps1
```

# Example of output

## Exclude Folders (default)

```csv
"C:\projects\reconciliation-files\random.txt","21"
"C:\projects\reconciliation-files\README.md","661"
"C:\projects\reconciliation-files\recfiles.ps1","968"
"C:\projects\reconciliation-files\reconciliation-files.csv","0"
```

## Include Folders

```csv
"FullName","Length"
"C:\projects\reconciliation-files\.git",
"C:\projects\reconciliation-files\Empty folder",
"C:\projects\reconciliation-files\Próxima Estación",
"C:\projects\reconciliation-files\Ελληνικό Όνομα",
"C:\projects\reconciliation-files\random.txt","21"
"C:\projects\reconciliation-files\recfiles.ps1","968"
"C:\projects\reconciliation-files\reconciliation-files.csv","0"
```
